module execute_tb;
	parameter N = 64; // N default value is 64 bits.
	logic AluSrc;
    logic [3:0] AluControl;
    logic [N-1:0] PC_E;
    logic [N-1:0] signImm_E;
    logic [N-1:0] readData1_E;
    logic [N-1:0] readData2_E;
    logic [N-1:0] PCBranch_E;
    logic [N-1:0] aluResult_E;
    logic [N-1:0] writeData_E;
    logic zero_E;

    execute #(N) dut(ALUSrc, AluControl, PC_E, signImm_E, readData1_E, readData2_E, PCBranch_E, aluResult_E, writeData_E, zero_E);
    
    initial begin
	
	AluSrc = 1;
	AluControl = 4'b0010;
	readData1_E = 64'd11;
	readData2_E = 64'd00;
	// aluResult_E debe dar 64'd11
	#10;
	
	AluSrc = 1;
	PC_E = 64'd15;
	signImm_E = 64'd2;
	// PCBranch_E debe dar 23
	#10;
	
	AluSrc = 0;
	signImm_E = 64'd23;
	// aluResult_E debe dar 46
	#10;
	
	end
				

endmodule