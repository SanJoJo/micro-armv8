module fetch_tb; 
	parameter N = 64; // N default value is 64 bits.
	logic clk;
	logic reset;
	logic PCSrc_F;
	logic [N-1:0] PCBranch_F;
	logic [N-1:0] imem_addr_F;
	
	fetch #(N) dut(clk, reset, PCSrc_F, PCBranch_F. imem_addr_F);
	
	always begin
		clk = 0; #10 ; clk = 1; #10 ;
	end
	
	initial begin
		
		PCBranch_F = 64'b0101;
		PCSrc_F = 0; 
		reset = 1; #5; 
		
		/*chequeamos que PC= PC + 4*/
		reset = 0; #5 ;
		
		PCSrc_F = 1;
		#10; PCBranch_F = 64'b1111;
		
		#20;
		$stop;
	
	end

endmodule