module regfile_tb();
	logic clk;
    logic we3;
    logic [4:0] ra1;
    logic [4:0] ra2;
    logic [4:0] wa3;
    logic [63:0] wd3;
    logic [63:0] rd1;
    logic [63:0] rd2;
    
    regfile dut(clk, we3, ra1, ra2, wa3, wd3, rd1, rd2);
    
    always begin
		clk = 1; #10; clk = 0; #10;
	end
    
    initial begin
    
		we3 = 1;
		wa3 = 25;
		wd3 = 25; #5;
		ra1 = 25; // registro 25 deberia ser 25
		#5;
		
		#10;
		we3 = 0;
		wa3 = 25;
		wd3 = 52; #5;
		ra1 = 25; // registro 25 deberia seguir valiendo 25
		#5;
		we3 = 1; #10; // hacemos que se escriba 
		
		#10;
		wa3 = 31;
		wd3 = 25;
		#5;
		ra2 = 31;
		#10;
		
		$stop;
		
	end
				
endmodule