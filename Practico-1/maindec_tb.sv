module maindec_tb;
	logic [10:0] Op;
	logic Reg2Loc;
	logic ALUSrc;
	logic MemtoReg;
	logic RegWrite;
	logic MemRead;
	logic MemWrite;
	logic Branch;
	logic [1:0] ALUOp;

	maindec dut(Op, Reg2Loc, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp);

	initial begin
	// LDUR
		Op = 11'b111_1100_0010;#20
	// STUR
		Op = 11'b111_1100_0000;#20
	// CBZ
		Op = 11'b101_1010_0111;#20
	// ADD
		Op = 11'b100_0101_1000;#20
	// SUB
		Op = 11'b110_0101_1000;#20
	// AND
		Op = 11'b100_0101_0000;#20
	// ORR
		Op = 11'b101_0101_0000;#20
		$stop;
	end
	
endmodule
