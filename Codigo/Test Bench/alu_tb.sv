/*
	Ejercicio 3: Unidad Aritmetica Logica (ALU).
	b) Test Bench.
*/
module alu_tb();

	logic [63:0]a;
	logic [63:0]b;
	logic [3:0]ALUControl;
	logic [63:0]result;
	logic zero;
	
	alu dut(a,b,ALUControl,result,zero);
	
	initial begin
	// Dos positivos
		a = 1998234; b = 1652342;
			ALUControl = 4'b0000; #10; // a AND b
			ALUControl = 4'b0001; #10; // a OR b
			ALUControl = 4'b0010; #10; // add (a+b)
			ALUControl = 4'b0110; #10; // sub (a-b)
			ALUControl = 4'b0111; #10; // pass input b
			ALUControl = 4'b1100; #10; // a NOR b
			ALUControl = 4'b1111; #10; // otherwise
	// Dos negativos
		a = -1998234; b = -1652342;
			ALUControl = 4'b0000; #10; // a AND b
			ALUControl = 4'b0001; #10; // a OR b
			ALUControl = 4'b0010; #10; // add (a+b)
			ALUControl = 4'b0110; #10; // sub (a-b)
			ALUControl = 4'b0111; #10; // pass input b
			ALUControl = 4'b1100; #10; // a NOR b
			ALUControl = 4'b1111; #10; // otherwise
	// Uno positivo y uni negativo
		a = 1998234; b = -1652342;
			ALUControl = 4'b0000; #10; // a AND b
			ALUControl = 4'b0001; #10; // a OR b
			ALUControl = 4'b0010; #10; // add (a+b)
			ALUControl = 4'b0110; #10; // sub (a-b)
			ALUControl = 4'b0111; #10; // pass input b
			ALUControl = 4'b1100; #10; // a NOR b
			ALUControl = 4'b1111; #10; // otherwise

		// Overflow
		a = '1; b = 1'b1;
		ALUControl = 4'b0010;
		#10;
		// Zero
		a = '0; b = '0;
		#10;
		
	end
endmodule