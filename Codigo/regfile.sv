/*
	Ejercicio 5: Register File (Banco de 32 registros).
	a) Implementacion.
*/
module regfile(
				input logic clk,
				input logic we3,
				input logic [4:0] ra1,
				input logic [4:0] ra2,
				input logic [4:0] wa3,
				input logic [63:0] wd3,
				output logic [63:0] rd1,
				output logic [63:0] rd2);
	
	logic [63:0] array_reg [0:31];
    
	initial begin
		for (int i=0;i<31;i++) array_reg[i] <= i;
		array_reg[31] = 0; // XZR
	end
	
	/*
	always @(negedge clk) begin
		rd1 <= array_reg[ra1];
		rd2 <= array_reg[ra2];
	end
	*/
	
	always @(negedge clk) begin
		if (we3 && wa3==ra1 ) begin
			rd1 <= wd3;
			rd2 <= array_reg[ra2]; 
		end
		else if (we3 && wa3==ra2) begin
			rd1 <= array_reg[ra1];
			rd2 <= wd3;
		end
		else begin
			rd1 <= array_reg[ra1];
			rd2 <= array_reg[ra2];
		end
	end
    
	always @(posedge clk) begin
		if(we3 && wa3 != 5'b11111) array_reg[wa3] <= wd3;
	end
endmodule
