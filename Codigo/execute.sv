module execute #(parameter N = 64) // N default value is 64 bits.
				(input logic AluSrc,
            input logic [3:0] AluControl,
            input logic [N-1:0] PC_E,
            input logic [N-1:0] signImm_E,
            input logic [N-1:0] readData1_E,
            input logic [N-1:0] readData2_E,
            output logic [N-1:0] PCBranch_E,
            output logic [N-1:0] aluResult_E,
            output logic [N-1:0] writeData_E,
            output logic zero_E);
				
				logic [N-1:0]sl2_result;
				
				sl2 Shift_Left_2(.a(signImm_E),.y(sl2_result));
				adder Add(.a(PC_E),.b(sl2_result),.y(PCBranch_E));
				
				logic [N-1:0] mux2_result;
				
				mux2 MUX(.d0(readData2_E),.d1(signImm_E),.s(AluSrc),.y(mux2_result));
				alu ALU(.a(readData1_E),.b(mux2_result),.ALUControl(AluControl),
							.result(aluResult_E),.zero(zero_E));
				
				assign writeData_E = readData2_E;
endmodule