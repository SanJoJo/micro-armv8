/*
	Ejercicio 7: Modulo Fetch.
	a) Implementacion.
*/
module fetch #(parameter N = 64) // N default value is 64 bits.
				(input logic clk,
				 input logic reset,
				 input logic PCSrc_F,
				 input logic [N-1:0] PCBranch_F,
				 output logic [N-1:0] imem_addr_F);
				 
	logic [N-1:0] adder_R;
	logic [N-1:0] d;
	logic [N-1:0] n4 = 4;
	
	mux2 MUX(.d0(adder_R),.d1(PCBranch_F),.s(PCSrc_F),.y(d));
	adder Add(.a(imem_addr_F),.b(n4),.y(adder_R));
	flopr PC(.clk(clk),.reset(reset),.d(d),.q(imem_addr_F));
		
endmodule
