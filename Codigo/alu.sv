/*
	Ejercicio 3: Unidad Aritmetica Logica (ALU).
	a) Implementacion.
*/
module alu(input logic [63:0]a,
			  input logic [63:0]b,
			  input logic [3:0]ALUControl,
			  output logic [63:0]result,
			  output logic zero);
			  
	logic [63:0]aux;
	always_comb
		case(ALUControl)
			4'b0000: aux = a&b;
			4'b0001: aux = a|b;
			4'b0010: aux = a+b;
			4'b0110: aux = a-b;
			4'b0111: aux = b;
			4'b1100: aux = ~(a|b);
			default: aux = a;
		endcase
	assign result = aux;
	assign zero = (aux == 64'b0) ? 1'b1 : 1'b0;
endmodule