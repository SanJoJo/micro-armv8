/*
	Ejercicio 4: Memoria ROM.
	b) Test Bench.
*/
module imem_tb();

	parameter N = 32;

	logic [5:0]addr; // Direccion de memoria.
	logic [N-1:0]q;
	
	// Instanciate Device Under Test (DUT).
	imem #(N) dut(addr, q);
	
	initial begin
		// Primeras 19 palabras:
		addr = 'h00; #10;
		addr = 'h01; #10;
		addr = 'h02; #10;
		addr = 'h03; #10;
		addr = 'h04; #10;
		addr = 'h05; #10;
		addr = 'h06; #10;
		addr = 'h07; #10;
		addr = 'h08; #10;
		addr = 'h09; #10;
		addr = 'h0A; #10;
		addr = 'h0B; #10;
		addr = 'h0C; #10;
		addr = 'h0D; #10;
		addr = 'h0E; #10;
		addr = 'h0F; #10;
		addr = 'h10; #10;
		addr = 'h11; #10;
		addr = 'h12; #10;
		// 6 palabras restantes:
		addr = 'h13; #10;
		addr = 'h14; #10;
		addr = 'h15; #10;
		addr = 'h16; #10;
		addr = 'h17; #10;
		addr = 'h18; #10;
	end
	
endmodule