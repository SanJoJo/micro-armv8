/*
	Ejercicio 1: Flip-Flop D con Reset Asincrono.
	b) Test Bench.
*/
module flopr_tb();

	parameter N = 64;

	// Internal variables.
	logic clk, reset;
	logic [N-1:0]d;
	logic [N-1:0]q;

	// Instanciate Device Under Test (DUT).
	flopr #(N) dut(clk, reset, d, q);

	// generate clock
	always // no sensitivity list, so it always executes
		begin
			clk = 0; #5; clk = 1; #5;
		end

		initial begin // At start of test pulse reset.
		reset = 0; d = '1; q = '0;
		#10; d = '0;
		#5; d = '1;
		#5; reset = 1;
		#10
		$stop;
	end
endmodule