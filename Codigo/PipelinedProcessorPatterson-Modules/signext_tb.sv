/*
	Ejercicio 2: Módulo de extension de signo.
	b) Test Bench.
*/
module signext_tb();
	// Internal variables.
	logic [31:0]a;
	logic [63:0]y;
	// Instanciate Device Under Test (DUT).
	signext dut (a,y);
	// generate clock
	initial begin
		// LDUR
		a = 32'b111_1100_0010_1100_0000_0000_0000_00000;#20;
		a = 32'b111_1100_0010_0110_0000_0000_0000_00000;#20;
		// STUR
		a = 32'b111_1100_0000_1111_0000_0000_0000_00000;#20;
		a = 32'b111_1100_0000_0001_0000_0000_0000_00000;#20;
		// CBZ
		a = 32'b101_1010_0111_1111_1111_1111_1111_11111;#20;
		a = 32'b101_1010_0011_0111_0111_0111_0111_01111;#20;
		// Instruciones no válidas
		a = '1;#20;
		a = 32'b111_1010_0111_1111_1111_1111_1111_11111;#20;
		$stop;
	end
endmodule