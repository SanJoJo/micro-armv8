/*
	Ejercicio 1: Flip-Flop D con Reset Asincrono.
	a) Implementacion.
*/

module flopr #(parameter N = 64) // N default value is 64 bits.
			  (input logic clk, // Clock.
			   input logic reset,
			   input logic [N-1:0] d,
			   output logic [N-1:0] q);
	
	// Asynchronous reset.
	always_ff @(posedge clk, posedge reset)
		if (reset) q <= '0;
		else q <= d;
endmodule