/*
	Ejercicio 4: Memoria ROM.
	a) Implementación.
*/
module imem #(parameter N = 32) // N default value is 32 bits.
			 (input logic [5:0]addr,
			  output logic [N-1:0]q);
	
	// Memoria
	logic [N-1:0] ROM [0:63] = '{default: 32'h0};
	initial begin
		// Instrucciones originales:
		/*
		ROM[0] = 32'hf8000000;
		ROM[1] = 32'hf8008001;
		ROM[2] = 32'hf8010002;
		ROM[3] = 32'hf8018003;
		ROM[4] = 32'hf8020004;
		ROM[5] = 32'hf8028005;
		ROM[6] = 32'hf8030006;
		ROM[7] = 32'hf8400007;
		ROM[8] = 32'hf8408008;
		ROM[9] = 32'hf8410009;
		ROM[10] = 32'hf841800a;
		ROM[11] = 32'hf842000b;
		ROM[12] = 32'hf842800c;
		ROM[13] = 32'hf843000d;
		ROM[14] = 32'hcb0e01ce;
		ROM[15] = 32'hb400004e;
		ROM[16] = 32'hcb01000f;
		ROM[17] = 32'h8b01000f;
		ROM[18] = 32'hf803800f;
		for (int i=19; i<64; i++) ROM[i] = 32'h0;
		*/
		// Instrucciones ejercicio 1:
		/*
		ROM[0] = 32'hf8000000;
		ROM[1] = 32'hf8008001;
		ROM[2] = 32'hf8010002;
		ROM[3] = 32'hf8018003;
		ROM[4] = 32'hf8020004;
		ROM[5] = 32'hf8028005;
		ROM[6] = 32'hf8030006;
		ROM[7] = 32'hf8400007;
		ROM[8] = 32'hf8408008;
		ROM[9] = 32'hf8410009;
		ROM[10] = 32'hf841800a;
		ROM[11] = 32'hf842000b;
		ROM[12] = 32'hf842800c;
		ROM[13] = 32'hf843000d;
		ROM[14] = 32'hcb0e01ce;
		ROM[15] = 32'h8b1f03ff;
		ROM[16] = 32'h8b1f03ff;
		ROM[17] = 32'hb40000ae;
		ROM[18] = 32'h8b1f03ff;
		ROM[19] = 32'h8b1f03ff;
		ROM[20] = 32'h8b1f03ff;
		ROM[21] = 32'hcb01000f;
		ROM[22] = 32'h8b01000f;
		ROM[23] = 32'h8b1f03ff;
		ROM[24] = 32'h8b1f03ff;
		ROM[25] = 32'hf803800f;
		for (int i=26; i<64; i++) ROM[i] = 32'h0;
		*/
		// Instrucciones ejercicio 2:
		
		ROM[0] = 32'h8b0403e0;
		ROM[1] = 32'h8b040001;
		ROM[2] = 32'h8b040022;
		ROM[3] = 32'h8b040043;
		ROM[4] = 32'hf80003e0;
		ROM[5] = 32'hf80083e1;
		ROM[6] = 32'hf80103e2;
		ROM[7] = 32'hf80183e3;
		for (int i=8; i<64; i++) ROM[i] = 32'h0;
		
		// Instrucciones ejercicio 2: Se cambia xzr por x0
		/*
		ROM[0] = 32'h8b0403e0;
		ROM[1] = 32'h8b1f03ff;
		ROM[2] = 32'h8b1f03ff;
		ROM[3] = 32'h8b040001;
		ROM[4] = 32'h8b1f03ff;
		ROM[5] = 32'h8b1f03ff;
		ROM[6] = 32'h8b040022;
		ROM[7] = 32'h8b1f03ff;
		ROM[8] = 32'h8b1f03ff;
		ROM[9] = 32'h8b040043;
		ROM[10] = 32'hf80003e0;
		ROM[11] = 32'hf80083e1;
		ROM[12] = 32'hf80103e2;
		ROM[13] = 32'hf80183e3;
		for (int i=14; i<64; i++) ROM[i] = 32'h0;
		*/
	end
		
	// Acceso a la memoria.
	assign q = ROM[addr];
endmodule