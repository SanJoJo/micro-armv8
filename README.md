# Proyectos Single Cycle Processor

## Arquitectura de Computadoras

### Integrantes

* __Santiago Balog__ ~ _santiagobalog1998@gmail.com_
* __Joel Farias__ ~ _joelfarias999@gmail.com_
* __Joel Molina__ ~ _jooelmolina21@gmail.com_

## Descripcion

El microprocesador ARMv8 trabaja con 64 bits.

## Herramientas

* Altera Quartus Prime Lite Edition
* Device Family:
  * Cyclone IV: EP4CE22F17C6
* EDA Tool Setting:
  * Simulation:
    * ModelSim-Alter
    * SystemVerilog HDL

## Estructura (Basica)

    .
    ├── ARMv8-SV/
    │   ├── db/
    │   ├── Lab-ARMv8.qpf
    │   └── Lab-ARMv8.qsf
    ├── Codigo/
    │   ├── armv8_createAssembly/
    │   │   ├── main.s
    │   │   ├── Makefile
    │   │   └── memmap
    │   ├── PipelinedProcessorPatterson-Modules/
    │   │   ├── adder.sv
    │   │   ├── alu_tb.sv
    │   │   ├── alu.sv
    │   │   ├── aludec.sv
    │   │   ├── controller.sv
    │   │   ├── datapath.sv
    │   │   ├── decode.sv
    │   │   ├── dmem.vhd
    │   │   ├── execute.sv
    │   │   ├── fetch.sv
    │   │   ├── flopr_tb.sv
    │   │   ├── flopr.sv
    │   │   ├── imem_tb.sv
    │   │   ├── imem.sv
    │   │   ├── maindec.sv
    │   │   ├── memory.sv
    │   │   ├── mux2.sv
    │   │   ├── processor_arm.sv
    │   │   ├── processor_tb.sv
    │   │   ├── regfile.sv
    │   │   ├── signext_tb.sv
    │   │   ├── signext.sv
    │   │   ├── sl2.sv
    │   │   └── writeback.sv
    │   ├── SingleCycleProcessorPatterson-Modules/
    │   │   ├── aludec.sv
    │   │   ├── controller.sv
    │   │   ├── datapath.sv
    │   │   ├── decode.sv
    │   │   ├── dmem.vhd
    │   │   ├── memory.sv
    │   │   ├── mux2.sv
    │   │   ├── processor_arm.sv
    │   │   ├── processor_tb.sv
    │   │   ├── sl2.sv
    │   │   └── writeback.sv
    │   ├── Test Bench/
    │   │   ├── alu_tb.sv
    │   │   ├── flopr_tb.sv
    │   │   ├── imem_tb.sv
    │   │   └── signext_tb.sv
    │   ├── adder.sv
    │   ├── alu.sv
    │   ├── execute.sv
    │   ├── fetch.sv
    │   ├── flopr.sv
    │   ├── imem.sv
    │   ├── maindec.sv
    │   ├── mux2.sv
    │   ├── regfile.sv
    │   ├── signext.sv
    │   └── sl2.sv
    ├── Practico-1/
    │   ├── db/
    │   ├── Practico1.qpf
    │   ├── Practico1.qsf
    │   └── Practico1.qws
    ├── Practico-2/
    │   ├── db/
    │   ├── Practico2.qpf
    │   ├── Practico2.qsf
    │   └── Practico2.qws
    ├── .gitignore
    └── README.md

## Extra

### Correcciones de Warnings

__Problema:__ _Warning (18236)_: Number of processors has not been specified which may cause overloading on shared machines. Set the global assignment NUM_PARALLEL_PROCESSORS in your QSF to an appropriate value for best performance.

__Solucion:__ Abrir el archivo _NombreDeProyecto.qsf en el directorio del proyecto y agregar `set_global_assignment -name NUM_PARALLEL_PROCESSORS ALL`.

__Problema:__  En la simulación sim:/processor_tb/DM_writeData aparecía siempre como 'xxxxxxxxxx...xxxxxx'
__Solucion:__  En el processor_tb se cambio la forma en la que se declara el _dut_ del procesador por lo siguiente: `processor_arm  dut (CLOCK_50, reset, dump, DM_writeData, DM_addr, DM_writeEnable);`
